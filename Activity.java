package com.zuitt.example;

import java.util.Scanner;
public class Activity {
    public static  void main(String[] args) {

        Scanner userInput = new Scanner(System.in);
        String firstName;
        String lastName;
        Double firstGrade;
        Double secondGrade;
        Double thirdGrade;
        int aveGrade;

        //first name
        System.out.println("First Name: ");
        firstName = userInput.nextLine();

        //last name
        System.out.println("Last Name: ");
        lastName = userInput.nextLine();

        //first grade
        System.out.println("First Subject Grade: ");
        firstGrade = userInput.nextDouble();

        //second grade
        System.out.println("Second Subject Grade: ");
        secondGrade = userInput.nextDouble();

        //third grade
        System.out.println("Third Subject Grade: ");
        thirdGrade = userInput.nextDouble();

        aveGrade = (int) ((firstGrade + secondGrade + thirdGrade) / 3);
        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + aveGrade);






    }
}
