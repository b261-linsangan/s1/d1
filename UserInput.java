package com.zuitt.example;

import java.util.Scanner;
public class UserInput {
    public static void main(String[] args) {
        //scanner is used to get user input from terminal
        //System.in allows us to take input from the console.
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter username: ");

        //to capture the input given by the user, we use the
        //nextLine method
        String userName = scan.nextLine();
        System.out.println("username is : "+userName);


    }
}
